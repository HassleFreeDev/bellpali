<?php get_header(); ?>

<div class="site-container page-height-cover">

        <div class="site-center site-padding-top">
            <h1>Stellenangebote/ Jobs</h1> <!-- Hardcoded sub-heading -->
            <hr class="site-main-underline"> <!-- Hardcoded underline -->
        </div>

        <?php
            // check if the repeater field has rows of data
            if( have_rows('add_job') ):
                
                // loop through the rows of data
                while ( have_rows('add_job') ) : the_row();

                $image = get_sub_field('stellenangebot_desktop'); // gets repeater sub field 'stellenangebot_desktop' 
                $file_mobile = get_sub_field('stellenangebot_mobile'); // gets repeater sub field 'stellenangebot_desktop'
                
            ?>
<!-- shown only on desktop -->       
                
                <div class="display-desktop-only image-box-size">
        
                    <h3 class="center-heading"><?php the_sub_field('job_bezeichnung'); ?>
                        <hr class="hr-sub-heading-jobs"> <!-- Hard coded underline for job offer -->
                    </h3>

                    <a href="<?php echo $file_mobile['url']; ?>">
                        <img class="image-stellenangebot" src="<?php echo $image['url']; ?>" alt="<?php echo esc_attr($image['alt']); ?>" /> <!-- Displays all, in the backend chosen, images -->
                    </a>
                </div>
                
<!-- shown only on smaller than desktop -->
                <div class="display-mobile-only">
                    <h3 class="center-heading"><a href="<?php echo $file_mobile['url']; ?>"><?php the_sub_field('job_bezeichnung'); ?></a></h3>
                    <hr class="hr-sub-heading-jobs"> <!-- Hard coded underline for end of whole Job offer -->
                </div>

              <?php endwhile;

            else : ?>

<!-- if there is no job offer, display the following paragraph -->
                <div class="justify-content">
                    <p>Leider gibt es derzeit keine offenen Stellen. Schau doch ein andermal wieder vorbei :) .</p>
                </div>
                

           <?php endif;

            ?>
<?php get_footer(); ?>