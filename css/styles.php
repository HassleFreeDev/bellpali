<?php
    header('Content-type: text/css; charset: UTF-8'); //switches Content from text/html to text/ css -> so i am able to use php in the stylesheet 
?>

.container::before {
    content: '';
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    height: <?php echo get_custom_header()->height; ?>px;
    background: url('<?php header_image(); ?>') top center no-repeat;
}



