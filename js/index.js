"use strict"

/* Navbar klappt sich beim Scrollen ein */

    var lastScrollTop = 0;

    jQuery(window).scroll(function(event){

    var st = jQuery(this).scrollTop();
    
    if (st > lastScrollTop){
        if (!jQuery('body').hasClass('down')) {
        jQuery('body').addClass('down');
        }
    } else {
        jQuery('body').removeClass('down');
    }

    lastScrollTop = st;

    if (jQuery(this).scrollTop() <= 0) {
        jQuery('body').removeClass('down');
    };
    });

/*
*==========================================================================================================
*/

/*
*   Code for flexslider, changing standard attributes
*/
    jQuery(window).on('load', function(){
        jQuery('.flexslider').flexslider({
            animation: "fade",
            slideshowSpeed: 7000,
            animationSpeed: 1200,
        
            start: function(slider){

            jQuery('body').removeClass('loading');
            }
        });
    });

/*
*==========================================================================================================
*/
    
/*
* Open lightbox (flexslider) after clicking on flexslider
*/

    var flexsliders = jQuery('.flexslider'); // get <div.flexslider>

    flexsliders.click(function() {
        var target = jQuery(this).data('target'); //get clicked object (specific example )
        var htmlElement = jQuery('#' + target);
        htmlElement.show();

        console.log(htmlElement);
    });
  
    var img = jQuery('.img');

    img.click(function() {
        var imgTarget = jQuery(this).data('target');
        var imgElement = jQuery('#' + imgTarget);
        imgElement.parents('.light-box').show();

        console.log(imgElement);
    });

// close button slides ( page-projekte.php and page-bike-store.php)
    jQuery('.site-close-btn-slider').click(function() {
        jQuery(this).parents('.site-overlay').hide();
    });
    
// close button slides ( page-projekte.php and page-bike-store.php)
jQuery('.site-close-btn-slider').click(function() {
    jQuery(this).parents('.light-box').hide();
});

// erfahre mehr button page-ueber-uns.php
    jQuery('#slide-btn-about').click(function() {
        jQuery('.plus-icon').toggleClass('plus-icon-clicked')
    });
    

    jQuery('#slide-btn-about').click(function() {
        jQuery('.learn-more-box').slideToggle('slow');
    });

// erfahre mehr button mobile 
    jQuery('.erfahre-mehr-mobile').click(function() {
        jQuery(this).next().slideToggle('slow');
    });

    jQuery('#close-btn').click(function() {
        jQuery('#site-nav-main-mobile').removeClass('site-nav-main-mobile-shown');
    });

    jQuery('#burger-btn').click(function() {
        jQuery('#site-nav-main-mobile').toggleClass("site-nav-main-mobile-shown");
    });

    jQuery('.more-box').click(function() {
        jQuery(this).next().slideToggle('slow');
    });

// shows background layout if burger is clicked
    jQuery('#burger-btn').click(function() {
        jQuery('.site-nav-layout-background-hidden').toggleClass("site-nav-layout-background-hidden site-nav-layout-background");
        jQuery('.site-nav-main-mobile-hidden').toggleClass("site-nav-main-mobile-hidden site-nav-main-mobile");
    });

// hides background layout if cross is clicked
    jQuery('#close-btn').click(function() {
        jQuery('.site-nav-layout-background').toggleClass("site-nav-layout-background site-nav-layout-background-hidden");
        jQuery('.site-nav-main-mobile').toggleClass("site-nav-main-mobile site-nav-main-mobile-hidden");
    });


/*
*==========================================================================================================
*/

    /*
    var subMenu = jQuery('.sub-menu');

    jQuery(this).hover(function () {
        subMenu.show();
        jQuery('.menu-item').children('.sub-menu').css('display: block');

        },

        function() {
            subMenu.hide();
        }
    );
    */

/*
*==========================================================================================================
*/

    /*
    EDIT: NOT NECESSARY - could be simple solved with css...
    
    jQuery('.menu-item').hover( function() { /* get .menu-item, then do following function: this (actual / focused .menu-item) -> the children (.sub-menu from relative poisioned .menu-item ) -> sho with its properties  
        jQuery(this).children('.sub-menu').show();
    },
    function() {
        jQuery('.sub-menu').hide();
    });
    */
    
/*
*==========================================================================================================
*/

// Adding/ removing a class to/from #page-nav-web-shop when moving from top position '0'

jQuery(function () {
    jQuery(window).scroll(function () {
        var top_offset = jQuery(window).scrollTop();
        if (top_offset == 0) {
            jQuery('#page-nav-web-shop').removeClass('fixed-top fade-in');
        } else {
            jQuery('#page-nav-web-shop').addClass('fixed-top fade-in');
        }
    })
}); 