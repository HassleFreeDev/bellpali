        

        </div>
        <footer style="clear: both;">
            <div class="site-footer site-layout-footer">
                <div class="site-center">

                    <p><?php the_field('text_field_footer_contact'); ?></p>
      
                    <p>&copy; <?php echo date("Y"); ?> Bellpali Motorradmanufaktur - All Rights reserved</p>
                    <div>
  
                        <?php get_template_part('template_parts/footer-navigation'); ?>


<!-- need to create and add a new menu, for impressum, agbs and so on, web shop hassle -->
 
                    </div>
                </div>

            </div>
        </footer>
        <?php wp_footer(); ?>
    </body>
</html>