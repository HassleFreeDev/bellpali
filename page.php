<?php get_header(); ?>
<div class="site-web-shop-container">
    <div class="page-web-shop-container">
        <div class="row">
            <div class="page-sidebar-container ">
                <div class="col-4 h-100">
                
                    <?php // create own wp_list_categories for woocommerce
                    $args = array(
                        'taxonomy'      => 'product_cat',
                        'title_li'      => '',
                        'show_count'    => true,
                        'container'     => '',
                        'orderby'       => 'name',
                        
                    ); ?>
                    
                    <?php

                    wp_list_categories($args); ?>
                    
                </div>
            </div>
            
            <div class="col-8">

                <div class="container-cart">   
            
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                        <?php the_content(); ?>

                    <?php endwhile;

                    else : ?>

                        <p><?php esc_html_e('Sorry, no posts matched your criteria.'); ?></p>

                    <?php endif; ?>
                    
                </div>

            </div>
            

        </div>
    </div>

</div>

<?php get_footer(); ?>