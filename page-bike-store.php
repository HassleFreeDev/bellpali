<?php get_header(); ?>
        <main>   

            <div class="site-container page-height-cover">     
                <div class="site-center site-padding-top">
                    <h1>FAHRZEUGBÖRSE</h1> <!-- Hardcoded sub-heading -->
                    <hr class="site-main-underline"> <!-- Hardcoded underline -->
                </div>
    
                <?php if ( is_page('bike-store') ) { /* insert slider-service.php if this is page-24.php*/
                    get_template_part('template_parts/slider', 'bikes'); 
                }; ?>
    
            </div>
        
        </main>

    <?php get_footer(); ?>