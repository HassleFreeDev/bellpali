<?php get_header(); ?>
<div class="site-web-shop-container">
    <div class="page-web-shop-container">
        <div class="row">
            <div class="page-sidebar-container ">
                <div class="col-lg-2 h-100">
                
                    <?php // create own wp_list_categories for woocommerce
                    $args = array(
                        'taxonomy'      => 'product_cat',
                        'title_li'      => '',
                        'show_count'    => true,
                        'container'     => '',
                        'orderby'       => 'name',
                        
                    ); ?>
                    
                    <?php

                    wp_list_categories($args); ?>
                    
                </div>
            </div>
            
            <div class="col-lg-10">

                <?php the_content(); ?>
                
            </div>

            

        </div>
    </div>

</div>

<?php get_footer(); ?>