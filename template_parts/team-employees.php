<?php
$i = 0;
while (have_rows('employee')) : the_row();

    $heading        = get_sub_field('heading');
    $image          = get_sub_field('image');
    $description    = get_sub_field('description');
    $content        = get_sub_field('content');
    $i++;
    ?>

    <div class="row info-contact">
        <div class="col-xl-3">
            <div class="page-border-box-about">

                <div class="site-center page-about-position-description">
                    <h4><?php echo $heading; ?></h4>
                </div>

                <img class="page-about-image-cover img-fluid lazyload" data-src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                <div class="site-center page-about-position-description">
                    <p><?php echo $description; ?></p>
                </div>
                <div class="show-me-on-mobile">
        <div class="more-box">Erfahre mehr</div>
            <div id="more-content-mobile" class="page-layout-about-description more-content-mobile">
                <p><?php echo $content; ?></p>
            </div>        
        </div>
            </div>
            
        </div>

        

        <div class="col-xl-9 show-me-on-desktop">

            <div class="page-border-box-about">
                <div class="page-layout-about-description">
                    <p><?php echo $content; ?></p>
                </div>
            </div>
        </div>

    </div>


    <?php endwhile; ?>