<?php if( have_rows('slider') ): ?> <!-- If statement, if there is an row, repeater, or anything else --> 

    <div class="flexslider"> <!-- Div for flexslider -->
        <ul class="slides">

            <?php while( have_rows('slider') ): the_row(); // if condition, : (and) while condition : do this 

                $image      = get_sub_field('image'); // gets repeater sub field 'image' --> to find in Site: Service --> Scroll to the bottom // to find in ACF --> Slideshow Service
                $imageurl   = $image['sizes']['slider']; // gets, in functions.php declared, the fix size of the images
                $caption    = get_sub_field('caption'); // like above, gets the sub field 'caption'

                ?>

                <li>
                    <img class="lazyload" data-src="<?php echo $imageurl; ?>" /> <!-- Displays all chosen images -->
                    <div class="flex-caption-service">  <!-- Design purpose -->
                        <p class=" slide-caption"><?php echo $caption; ?></p> <!-- Displays all chosen captions -->
                    </div>
                </li>

            <?php endwhile; ?>
        </ul>
    </div>

<?php endif; ?>

