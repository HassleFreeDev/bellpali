<div class="site-nav-category">

    <?php // generates menu where you can change the shown sites (in the nav element) in the backend

            $args = array(
                'theme_location'    => 'sidebar_cart',
                'depth'             => 1, // how deep is an list shown like 'program -> new -> romantic' - it is only 'program -> new' shown
                'container'         => '',
                'menu_class'        => 'sidebar-cart' //set an class you can work with css
            );


            wp_nav_menu($args);

    ?>

</div>

