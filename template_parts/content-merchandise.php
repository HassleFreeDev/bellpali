<?php if (the_content()) : ?>
    <div class="content-merch">

        <?php the_content(); ?>

    </div>
<?php endif; ?>


<?php

if (have_rows('merch')) :

    $i = 0;
    while (have_rows('merch')) : the_row();


        $image_first            = get_sub_field('image_first');
        $content_first          = get_sub_field('content_first');
        $images_slider_first    = get_sub_field('image_slider_first');

        $image_second           = get_sub_field('image_second');
        $content_second         = get_sub_field('content_second');
        $images_slider_second   = get_sub_field('image_slider_second');

        $i++;
        ?>


        <div class="row page-product-box-layout">



            <div class="col-xl-6 page-product-width">
                <?php if ($image_first) : ?>
                    <div class="row">
                        <!-- Image First -->
                        <div class="col-xl-6">
                            <div class="page-merch-layout-box">

                                <img class="page-merch-image img-fluid img lazyload" data-src="<?php echo $image_first['url']; ?>" alt="<?php echo $image_first['alt']; ?>" data-target="first-img-<?php echo $i; ?>" /> <!-- data-target referres to following img tag -->
                                <div class="light-box">

                                    <div class="site-overlay-merch" id="first-img-<?php echo $i; ?>">
                                        <!-- id needs to the same like data-target above!!!! -->

                                        <div class="site-close-btn-slider" id="close"></div>

                                        <div class="page-overlay-merch">
                                            <div class="flexslider">
                                                <!-- Div for flexslider -->
                                                <ul class="slides">

                                                    <?php foreach ($images_slider_first as $image) : ?>
                                                        <li>
                                                            <img class="lazyload" data-src="<?php echo ($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                                                        </li>
                                                    <?php endforeach; ?>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="show-me-on-mobile">
                                    <div class="more-box">Erfahre mehr</div>
                                    <div id="more-content-mobile" class="more-content-mobile">

                                        <?php echo $content_first; ?>

                                    </div>
                                </div>
                            </div>

                        </div>

                        <!-- Content First -->
                        <div class="col-xl-6 page-merch-product-box-layout">
                            <div class="page-merch-layout-box show-me-on-desktop">
                                <p><?php echo $content_first; ?></p>
                            </div>
                        </div>

                    </div>

                <?php endif; ?>
            </div>

            <!-- First Row End -->

            <div class="col-xl-6 page-product-width">
                <?php if ($image_second) : ?>
                    <div class="row">
                        <!-- Image Second -->
                        <div class="col-xl-6">
                            <div class="page-merch-layout-box">

                                <img class="page-merch-image img-fluid img lazyload" data-src="<?php echo $image_second['url']; ?>" alt="<?php echo $image_second['alt']; ?>" data-target="second-img-<?php echo $i; ?>" />
                                <div class="light-box">

                                    <div class="site-overlay-merch" id="second-img-<?php echo $i; ?>">
<!-- id needs to the same like data-target above!!!! -->

                                        <div class="site-close-btn-slider" id="close"></div>

                                        <div class="page-overlay-merch">
                                            <div class="flexslider">
<!-- Div for flexslider -->
                                                <ul class="slides">

                                                    <?php foreach ($images_slider_second as $image) : ?>
                                                        <li>
                                                            <img class="lazyload" data-src="<?php echo ($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                                                        </li>
                                                    <?php endforeach; ?>

                                                </ul>
                                            </div>
                                        </div>

                                </div>

                            </div>

                            <div class="show-me-on-mobile">
                                <div class="more-box">Erfahre mehr</div>
                                <div id="more-content-mobile" class="more-content-mobile">
                                    <?php echo $content_second; ?>
                                </div>
                            </div>
                        </div>


                    </div>

                    <!-- Content Second -->
                    <div class="col-xl-6 page-merch-product-box-layout">
                        <div class="page-merch-layout-box show-me-on-desktop">
                            <p><?php echo $content_second; ?></p>
                        </div>
                    </div>

            </div>
        <?php endif; ?>
        </div>
        <!-- Second Row End -->

        </div>

    <?php endwhile; ?>


<?php else : ?>

    <div class="container container-layout">

        <p class="p-else">Derzeit ist kein Merchandise verfügbar. <br></p>
        <p class="p-else-2">Schau doch später noch einmal rein.</p>
    </div>


<?php endif; ?>