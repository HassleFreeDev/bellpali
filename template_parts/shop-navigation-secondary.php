<div class="shop-navigation-secondary-container">

    <?php // generates menu where you can change the shown sites (in the nav element) in the backend

                $args = array(
                    'theme_location'    => 'nav_shop_secondary',
                    'depth'             => 1, // how deep is an list shown like 'program -> new -> romantic' - it is only 'program -> new' shown
                    'container'         => '',
                    'menu_class'        => 'site-shop-nav-secondary', //set an class you can work with css
                );
                

                wp_nav_menu($args);
                
        ?>

</div>

    