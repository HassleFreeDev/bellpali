<?php
// check if the repeater field has rows of data

$i = 0; // sets an counter
if (have_rows('slider')) :

    // loop through the rows of data
    while (have_rows('slider')) : the_row();

        $main_image     = get_sub_field('main_image'); // gets repeater sub field 'stellenangebot_desktop'
        $caption        = get_sub_field('caption');
        $price          = get_sub_field('price');
        $images        = get_sub_field('slides'); // gets repeater sub field 'stellenangebot_desktop'
        $i++; // defines, that counter starts counting

        ?>

        <div class="page-position-flexslider-bikes">
            <div class="flexslider flexslider-hover flexslider-bikes" data-target="slide-<?php echo $i; ?>">
                <!-- put counter here to let different slider happen -->
                <!-- Div for flexslider -->
                <ul class="slides slides-bikes">

                    <li>
                        <img  class="lazyload" data-src="<?php echo $main_image['url']; ?>" /> <!-- Displays all, in the backend, chosen images -->

                        <div class="show-me-on-desktop">
                            <div class="flex-caption-bikes">
                                <!-- Design purpose -->
                                <?php echo $caption; ?>
                                <!-- Displays all, in the backend, chosen captions -->

                                <?php if (is_page('bike-store')) { ?>

                                    <div class="price">
                                        <p class="price-string">Preis:</p>
                                        <p class="price-integer"><?php echo $price; ?></p>
                                    </div>
                            </div>

                        <?php } else { ?>

<!-- display nothing when not page-bike-store.php (no price at least) -->

                        <?php } ?>
                        </div>
                    </li>


                </ul>

            </div>
            <div class="show-me-on-mobile">
                <div class="erfahre-mehr-mobile">

                    <?php if (is_page('projekte')) { ?>
                        <span class="give-me-more-mobile" id="slide-btn-about-mobile">Informationen</span>
                    <?php } else { ?>
                        <span class="give-me-more-mobile" id="slide-btn-about-mobile">Informationen und Preis</span>
                    <?php } ?>

                </div>
                <div id="learn-more-box" class="learn-more-box">
                <hr class="site-main-underline site-margin-bot show-me-on-desktop"> <!-- Hardcoded underline -->

                <div class="page-layout-bike-informations-mobile">
                    <?php echo $caption; ?>
                    <!-- Displays all, in the backend, chosen captions -->

                    <?php if (is_page('bike-store')) { ?>

                        <div class="price-mobile">
                            <p class="price-string-mobile">Preis:</p>
                            <p class="price-integer-mobile"><?php echo $price; ?></p>
                        </div>

                    <?php } else { ?>

                        <!-- display nothing when not page-bike-store.php (no price at least) -->

                    <?php } ?>
                </div>

            </div>
            </div>

            
        </div>

        <!-- hidden carousel, wich gets triggered when <img> is clicked -->
        <div class="site-overlay" id="slide-<?php echo $i; ?>">
            <!-- id needs to the same like data-target above!!!! -->

            <div class="site-close-btn-slider" id="close"></div>

            <div class="page-overlay-bikes">
                <div class="flexslider">
                    <!-- Div for flexslider -->
                    <ul class="slides">

                        <?php foreach ($images as $image) : ?>
                            <li>
                                <img class="lazyload" data-src="<?php echo ($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                            </li>
                        <?php endforeach; ?>

                    </ul>
                </div>
            </div>
        </div>
        <hr class="page-hr-bikes"> <!-- Hardcoded underline -->

    <?php endwhile; ?>

<?php else : ?>

    <?php get_template_part('template_parts/content', 'error-slider'); ?>

<?php endif;

?>







<!-- Old slider markup - DEPRECIATED -->
<!-- 
< ?php if( have_rows('slider_bikes_1') ): ?> <-- If statement, if there is an row, repeater, or anything else -->



<!-- 
    <div class="page-position-flexslider-bikes">
        <div class="flexslider flexslider-hover flexslider-bikes" data-target="bikes"> <-- Div for flexslider 
            <ul class="slides slides-bikes">
            
                < ?php while( have_rows('slider_bikes_1') ): the_row(); // If condition, : (and) while condition : do this 

                    $image      = get_sub_field('image'); // gets repeater sub field 'image' -> to find in Site: Service -> Scroll to the bottom // to find in ACF -> Slideshow Service
                    $imageurl   = $image['sizes']['slider_bikes']; // gets, in functions.php declared, the fix size of the images 
                    ?>

                    <li>
                        <img src="< ?php echo $imageurl; ?>" /> <-- Displays all, in the backend, chosen images ->
                        <div class="flex-caption-bikes">  <-- Design purpose ->
                            <p class=" slide-caption">< ?php the_field('caption_1'); ?></p> <-- Displays all, in the backend, chosen captions ->
                        </div>
                    </li>

                < ?php endwhile; ?>
            </ul>
        </div>
    </div>
    
<hr class="page-hr-bikes"> <-- Hardcoded underline ->

< ?php endif; ?>

<-- End Slider 1
================================================================================================================================================================================================
-->