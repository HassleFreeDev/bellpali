

<?php if (is_page('stellenangebote-jobs')) { ?>

    <div class="justify-content">
        <p>Leider gibt es derzeit keine offenen Stellen. Schau doch ein andermal wieder vorbei :) .</p>
    </div>

<?php } else { ?>

    <div class="justify-content">
        <p>Leider gibt es derzeit keine neuen Projekte. Schau doch ein andermal wieder vorbei :) .</p>
    </div>

<?php } ?>
