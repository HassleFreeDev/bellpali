<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * Grundeinstellungen für WordPress
 *
 * Zu diesen Einstellungen gehören:
 *
 * * MySQL-Zugangsdaten,
 * * Tabellenpräfix,
 * * Sicherheitsschlüssel
 * * und ABSPATH.
 *
 * Mehr Informationen zur wp-config.php gibt es auf der
 * {@link https://codex.wordpress.org/Editing_wp-config.php wp-config.php editieren}
 * Seite im Codex. Die Zugangsdaten für die MySQL-Datenbank
 * bekommst du von deinem Webhoster.
 *
 * Diese Datei wird zur Erstellung der wp-config.php verwendet.
 * Du musst aber dafür nicht das Installationsskript verwenden.
 * Stattdessen kannst du auch diese Datei als wp-config.php mit
 * deinen Zugangsdaten für die Datenbank abspeichern.
 *
 * @package WordPress
 */

// ** MySQL-Einstellungen ** //
/**   Diese Zugangsdaten bekommst du von deinem Webhoster. **/

/**
 * Ersetze datenbankname_hier_einfuegen
 * mit dem Namen der Datenbank, die du verwenden möchtest.
 */
define( 'DB_NAME', 'w014d647_BP' );

/**
 * Ersetze benutzername_hier_einfuegen
 * mit deinem MySQL-Datenbank-Benutzernamen.
 */
define( 'DB_USER', 'Bellpali' );

/**
 * Ersetze passwort_hier_einfuegen mit deinem MySQL-Passwort.
 */
define( 'DB_PASSWORD', '85D*ge2v' );

/**
 * Ersetze localhost mit der MySQL-Serveradresse.
 */
define( 'DB_HOST', 'localhost:3306' );

/**
 * Der Datenbankzeichensatz, der beim Erstellen der
 * Datenbanktabellen verwendet werden soll
 */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Der Collate-Type sollte nicht geändert werden.
 */
define('DB_COLLATE', '');

/**#@+
 * Sicherheitsschlüssel
 *
 * Ändere jeden untenstehenden Platzhaltertext in eine beliebige,
 * möglichst einmalig genutzte Zeichenkette.
 * Auf der Seite {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * kannst du dir alle Schlüssel generieren lassen.
 * Du kannst die Schlüssel jederzeit wieder ändern, alle angemeldeten
 * Benutzer müssen sich danach erneut anmelden.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ':/sE3X*nqnoE{hj_{PP4=JML)QtEqJ_#TTWop+Gy4ls-M0E]wLlskb;_/?E6.yHZ' );
define( 'SECURE_AUTH_KEY',  ':)_K&Xe4kqTzYew^UHlqN{1_7IRJ0)+]_(:@CVJXU9)0+cR+}JW3`i;HO]XW,6M5' );
define( 'LOGGED_IN_KEY',    'L#orbu9|u@BHa#|AXqjCw00(}:*od^>~qKa;/ 9Ms-73Q_(7x}XFxL_t@+okO4wL' );
define( 'NONCE_KEY',        '[3f;.y+FUV$4r+jr}l=+k(A,=1DD=fK{iO4,a/l_Iaf`,tMsOjWh#IPB6+(Zpp W' );
define( 'AUTH_SALT',        'k[_PAOaQDtAAn05|db)&%=i,PQc4HFIg{^a^QPHJuIo>UBt @@!|pu(ihk^bdom!' );
define( 'SECURE_AUTH_SALT', 'D3TWXMJ&gyHAys4v<7-,BcgmtO0fQBv%CjHUPVv6~KbaS15.WHQU0KG;fU+1Ca|M' );
define( 'LOGGED_IN_SALT',   'xp.!;@Bn(PmJHL`3Cy0x`~L=4pa`Y;EmPBWn]s?PG0Q(JIYcu5mtAjj4$myU8W`W' );
define( 'NONCE_SALT',       'A6,>d{;d9d<V-GVc&ZJ$IAKuHISn>fLXZgriifl:H#4.~Wj1I,2zoTm|=rtP/xgL' );

/**#@-*/

/**
 * WordPress Datenbanktabellen-Präfix
 *
 * Wenn du verschiedene Präfixe benutzt, kannst du innerhalb einer Datenbank
 * verschiedene WordPress-Installationen betreiben.
 * Bitte verwende nur Zahlen, Buchstaben und Unterstriche!
 */
$table_prefix = 'bp_';

/**
 * Für Entwickler: Der WordPress-Debug-Modus.
 *
 * Setze den Wert auf „true“, um bei der Entwicklung Warnungen und Fehler-Meldungen angezeigt zu bekommen.
 * Plugin- und Theme-Entwicklern wird nachdrücklich empfohlen, WP_DEBUG
 * in ihrer Entwicklungsumgebung zu verwenden.
 *
 * Besuche den Codex, um mehr Informationen über andere Konstanten zu finden,
 * die zum Debuggen genutzt werden können.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */

define( 'WP_DEBUG', true );

define('WP_ALLOW_MULTISITE', true);

define( 'JETPACK_DEV_DEBUG', true );

/* Das war’s, Schluss mit dem Bearbeiten! Viel Spaß. */
/* That's all, stop editing! Happy publishing. */

/** Der absolute Pfad zum WordPress-Verzeichnis. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Definiert WordPress-Variablen und fügt Dateien ein.  */
require_once( ABSPATH . 'wp-settings.php' );


