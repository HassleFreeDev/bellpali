<?php get_header(); ?>
<main>


    <?php
        $image_left = get_field('image_left_card');
        $image_middle_left = get_field('image_middle_left_card');
        $image_middle = get_field('image_middle_card');
        $image_middle_right = get_field('image_middle_right_card');
        $image_right = get_field('image_right_card'); ?>

    

    <div class="site-container page-service__content-selector">

        <div class="site-center site-padding-top">
            <h1>ALLES WAS DU BRAUCHST</h1> <!-- Hardcoded sub-heading -->
            <hr class="site-main-underline"> <!-- Hardcoded underline -->
        </div>
        <!-- Add ACF image_field (in this case) -->
        <div class="show-me-on-desktop">
            <div class="site-center page-service-top-image">
                <?php

                $image = get_field('image_field_service_top');

                if (!empty($image)) : ?>

                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                <?php endif; ?>
                
                <h1 class="page-service-caption-position"><?php the_field('text_field_service_caption_top'); ?></h1> <!-- adjustable caption above the image -->
            </div>
        </div>

        <div class="card-group site-center">
            <!-- card one (left) with :hover, and text_fields -->
            <div class="show-me-on-mobile card-no-margin">
                <img src="<?php echo esc_url($image_left['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" alt="">
            </div>
            <div class="card card-hover1">
                <div class="card-body">
                    <h3 class="card-title"><?php the_field('heading_left_card'); ?></h3>
                    <p class="card-text"><?php the_field('content_left_card'); ?></p>
                </div>
            </div>

            <!-- card two with (middle left) :hover, and text_fields -->
            <div class="show-me-on-mobile">
                <img src="<?php echo esc_url($image_middle_left['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" alt="">
            </div>
            <div class="card card-hover2">
                <div class="card-body">
                    <h3 class="card-title"><?php the_field('heading_middle_left_card'); ?></h3>
                    <p class="card-text"><?php the_field('content_middle_left_card'); ?></p>
                </div>
            </div>

            <!-- card two with (middle left) :hover, and text_fields -->
            <div class="show-me-on-mobile">
                <img src="<?php echo esc_url($image_middle['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" alt="">
            </div>
            <div class="card card-hover3">
                <div class="card-body">
                    <h3 class="card-title"><?php the_field('heading_middle_card'); ?></h3>
                    <p class="card-text"><?php the_field('content_middle_card'); ?></p>
                </div>
            </div>

            <!-- card three (middle right) with :hover, and text_fields -->
            <div class="show-me-on-mobile"> 
                <img src="<?php echo esc_url($image_middle_right['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" alt="">
            </div>
            <div class="card card-hover4">
                <div class="card-body">
                    <h3 class="card-title"><?php the_field('heading_middle_right_card'); ?></h3>
                    <p class="card-text"><?php the_field('content_middle_right_card'); ?></p>
                </div>
            </div>

            <!-- card four (right) with :hover, and text_fields -->
            <div class="show-me-on-mobile"> 
                <img src="<?php echo esc_url($image_right['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" alt="">
            </div>
            <div class="card card-hover5">
                <div class="card-body">
                    <h3 class="card-title"><?php the_field('heading_right_card'); ?></h3>
                    <p class="card-text"><?php the_field('content_right_card'); ?></p>
                </div>
            </div>
        </div>

        <div class="show-me-on-desktop">
            <?php if (is_page('service')) { /* insert slider-service.php if is page-service.php*/
                get_template_part('template_parts/slider', 'service');
            }; ?>
        </div>

    </div>

</main>

<hr class="site-hr-footer">

<?php get_footer(); ?>