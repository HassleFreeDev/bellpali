<?php get_header(); ?>
        <main>

        <div class="site-container container-contact-mobile">
            <div class="site-center site-padding-top">
                <h1>KONTAKT / ÖFFNUNGSZEITEN</h1> <!-- Hardcoded sub-heading -->
                <hr class="site-main-underline"> <!-- Hardcoded underline -->
            </div>
            <section>
<!-- ACF Contact Part Start-->
            <div class="row info-contact">
                <div class="col-xl-4 page-border-image-box">

                    <?php 

                        $image = get_field('top_image_field_contact'); 

                        if( !empty($image) ): ?>

                        <img class="show-me-on-desktop" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                    <?php endif; ?>

                </div>

                <div class="col-xl-8">
                    <div class="page-border-info-box-contact"> <!-- class set seperate in a div, because the width of the boxes wouldn't align (first block is 5 px more left than the second block, and so on) -->
                        <div class="site-center">
                            <h3 class="heading-contact">Schau vorbei und erlebe BELLPALI</h3>
                            <p>Du findest uns auf dem Gelände des Faradit Gewerbeparks, direkt am Südring.</p>
                        </div>
                        <div class="site-center info-text-contact">  
                               <strong><?php the_field('text_field_adress_contact') ?></strong>
                        </div>
                    </div>
                </div>
            </div>
<!-- ACF Contact Part End-->
            </section>
<!-- ACF Mail Part Start-->
            <div class="row info-contact">
                <div class="col-xl-8">
                    <div class="page-border-info-box-contact"> <!-- class set seperate in a div, because the width of the boxes wouldn't align (first block is 5 px more left than the second block, and so on) -->
                        <div class="site-center">
                            <h3 class="heading-contact">Keine Zeit, willst aber vorher wissen ob wir helfen können?</h3>
                            <p>Schreib uns.</p>
                        </div>

                        <div class="site-center info-text-contact ">  
                            <b>
                                <?php the_field('text_field_mail_contact') ?>
                            </b>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4">

                    <?php 

                        $image = get_field('middle_image_field_mail_contact'); 

                        if( !empty($image) ): ?>

                        <img class="show-me-on-desktop" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                    <?php endif; ?> 

                </div>
            </div>
<!-- ACF Mail Part End-->

<!-- ACF Phone Part Start-->
<div class="row info-contact">
                <div class="col-xl-4 page-border-image-box">

                    <?php 

                        $image = get_field('middle_image_field_phone_contact'); 

                        if( !empty($image) ): ?>

                        <img class="show-me-on-desktop" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                    <?php endif; ?>

                </div>

                <div class="col-xl-8">
                    <div class="page-border-info-box-contact"> <!-- class set seperate in a div, because the width of the boxes wouldn't align (first block is 5 px more left than the second block, and so on) -->
                        <div class="site-center">
                            <h3 class="heading-contact">Schriftlich reicht dir nicht?</h3>
                            <p>Ruf uns an.</p>
                        </div>

                        <div class="site-center info-text-contact">  
                            <b>
                                <?php the_field('text_field_phone_contact') ?>
                            </b>
                        </div>
                    </div>
                </div>
            </div>
<!-- ACF Phone Part End-->

<!-- ACF Mail Part Start-->
            <div class="row info-contact">
                <div class="col-xl-8">
                    <div class="page-border-info-box-business-hours"> <!-- class set seperate in a div, because the width of the boxes wouldn't align (first block is 5 px more left than the second block, and so on) -->
                        <div class="site-center">
                            <h3 class="heading-contact">Öffnungszeiten</h3>
                        </div>

    <!-- SUMMER_LEFT Because of layout reasons I have created two text_fields, one for the specific day, and one for the business hours -->
                        <div class="info-text-business-hours-left-contact">  
                            
                            <?php the_field('text_field_business_hours_summer_left') ?>
                            
                        </div>

    <!-- SUMMER_RIGHT Because of layout reasons I have created two text_fields, one for the specific day, and one for the business hours -->
                        <div class="info-text-business-hours-right-contact">
                            
                            <?php the_field('text_field_business_hours_summer_right') ?>
                            
                        </div>

                        <div class="site-center">
                            <h3 class="heading-contact">Winteröffnungszeiten</h3>
                        </div>

    <!-- WINTER_LEFT Because of layout reasons I have created two text_fields, one for the specific day, and one for the business hours -->
                        <div class="info-text-business-hours-left-contact">  
                            
                                <?php the_field('text_field_business_hours_winter_left') ?>
                            
                        </div>

    <!-- WINTER_RIGHT Because of layout reasons I have created two text_fields, one for the specific day, and one for the business hours -->
                        <div class="info-text-business-hours-right-contact-winter">  
                            
                                <?php the_field('text_field_business_hours_winter_right') ?>
                            
                        </div>
                    </div>
                </div>

                <div class="col-xl-4">
                    
                    <div class="page-border-info-box-parent">   
                        <?php 

                            $image = get_field('bottom_image_field_business_hours_contact'); 

                            if( !empty($image) ): ?>

                            <img class="show-me-on-desktop" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                        <?php endif; ?> 
                        
                        <div class="page-border-info-box-business-hours-info-box"> <!-- awkward class name...  -->

                            <div class="site-center page-layout-info-box-contact-top">
                                <b><?php the_field('text_field_info_box_bold'); ?></b>
                            </div>

                            <div class="site-center page-layout-info-box-contact-bottom">
                                <?php the_field('text_field_info_box_normal'); ?>
                            </div>

                        </div>

                    </div>

                </div>
                    
            </div>
<!-- ACF Mail Part End-->

            <hr class="site-hr-footer">
        </div>

        </main>
    <?php get_footer(); ?>
    
    