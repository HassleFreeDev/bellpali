<?php get_header(); ?>
<main>

<?php if (have_rows('info_box')) : ?>


<?php while (have_rows('info_box')) : the_row();

        $heading            = get_sub_field('heading');
        $topic_description  = get_sub_field('topic_description');
        $when               = get_sub_field('when');
        $location           = get_sub_field('location');
        $topic              = get_sub_field('topic');
        $costs              = get_sub_field('costs');
        $main_informations  = get_sub_field('main_informations');
        $participants       = get_sub_field('participants');
        $additional_content = get_sub_field('additional_content');


        ?>

    <div class="container-news">

        <div class="sub-container-news">
            <h2><?php echo $heading; ?></h2>

            <div class="news-content">
                <span class="topic-description"><?php echo $topic_description; ?></span>

                <div class="row row-height">
                    <div class="col-1">Wann:</div>
                    <div class="col-10"><?php echo $when; ?></div>
                </div>
                <div class="row row-height">
                    <div class="col-1">Wo:</div>
                    <div class="col-10"><?php echo $location; ?></div>
                </div>
                <div class="row row-height">
                    <div class="col-1">Thema:</div>
                    <div class="col-10"><?php echo $topic; ?></div>
                </div>
                <div class="row row-height">
                    <div class="col-1">Kosten:</div>
                    <div class="col-10"><?php echo $costs; ?></div>
                </div>



                <span class="participants"><?php echo $participants; ?></span>

                <span class="additional-content"><?php echo $additional_content; ?></span>
            </div>

        </div>
    </div>

<?php endwhile; ?>

<?php else : ?>

<?php endif; ?>

    <div class="container-news-mobile">
        <div class="site-container">
            <div class="sub-container-news-mobile">
                <div class="site-center">
                    <h2><?php echo $heading; ?></h2>
                </div>

                <div class="news-content">
                    <div class="site-center">
                        <span class="topic-description-mobile"><?php echo $topic_description; ?></span>
                    </div>
                    <div class="row">
                        <div class="col-2">Wann:</div>
                        <div class="col-10"><?php echo $when; ?></div>
                    </div>
                    <div class="row">
                        <div class="col-2">Wo:</div>
                        <div class="col-10"><?php echo $location; ?></div>
                    </div>
                    <div class="row">
                        <div class="col-2">Thema:</div>
                        <div class="col-10"><?php echo $topic; ?></div>
                    </div>
                    <div class="row">
                        <div class="col-2">Kosten:</div>
                        <div class="col-10 display-inline"><?php echo $costs; ?></div>
                    </div>
                    <div class="site-center">
                        <span class="participants"><?php echo $participants; ?></span>
                    </div>
                    <span class="additional-content-mobile"><?php echo $additional_content; ?></span>
                </div>

            </div>
        </div>
    </div>
    <div class="site-container">
        <!-- ACF = Advanced Custom Fields -->

        <div class="site-center site-padding-top">
            <h1>INDIVIDUALITÄT</h1> <!-- Hardcoded sub-heading -->
            <hr class="site-main-underline"> <!-- Hardcoded underline -->
        </div>


        <!-- First text / image part -->
        <!-- Add ACF top_image_field_top_layer -->

        <div class="row container-images">
            <div class="col-xl-6">
                <div class="page-image-top-layer">

                    <?php

                    $image = get_field('top_image_field_top_layer');

                    if (!empty($image)) : ?>

                        <img class="lazyload" data-src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                    <?php endif; ?>

                </div>

                <!-- Add ACF top_image_field_bottom_layer -->
                <div class="page-image-bottom-layer">

                    <?php

                    $image = get_field('top_image_field_bottom_layer');

                    if (!empty($image)) : ?>

                        <img class="lazyload" data-src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                    <?php endif; ?>

                </div>
            </div>

            <!-- Add ACF text_field_top -->

            <div class="col-xl-6">
                <div class="site-center">


                    <a class="page-sub-heading-home" href="<?php echo get_permalink(apply_filters('wpml_object_id', 2, 'post', true)); ?>"><?php _e('JEDER IST SEINES GLÜCKES SCHMIED!', ''); ?></a>

                    <p>
                        <b>Und wir sind dein Hammer!</b>
                    </p>
                </div>
                <div class="page-text-field-layout-home">
                    <?php the_field('text_field_top'); ?>
                </div>

            </div>
        </div>


        <!-- Second text / image part -->
        <!-- Second ACF (text_field_middle and middle_image_field_...) -->

        <div class="site-center site-padding-top">
            <h1>DO IT YOURSELF</h1> <!-- Hardcoded sub-heading -->
            <hr class="site-main-underline"> <!-- Hardcoded underline -->
        </div>

        <!-- Display on Desktop only -->
        <!-- Add ACF text_field_middle -->
        <div class="show-me-on-desktop">
            <div class="row container-images ">

                <div class="col-xl-6">

                    <div class="site-center">

                        <!-- CHANGE SITE ID TO CORRECT SITE; FALSE ID IS SELECTED ATM!!! -->
                        <h3 class="page-sub-heading-home" href="<?php echo get_page('about.php'); ?>"><?php _e('MACH DIR DIE HÄNDE SCHMUTZIG!', ''); ?></h3>
                        <!-- CHANGE SITE ID TO CORRECT SITE; FALSE ID IS SELECTED ATM!!! -->

                        <p>
                            <b>Mit einer Professionellen Ausstattung!</b>
                        </p>

                    </div>

                    <div class="page-text-field-layout-home">
                        <?php the_field('text_field_middle'); ?>
                    </div>

                </div>

                <div class="col-xl-6">

                    <!-- Add ACF middle_image_field_top_layer -->

                    <div class="page-image-top-layer">

                        <?php

                        $image = get_field('middle_image_field_top_layer');

                        if (!empty($image)) : ?>

                            <img class="lazyload" data-src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                        <?php endif; ?>

                    </div>
                    <!-- Add ACF middle_image_field_bottom_layer -->
                    <div class="page-image-bottom-layer">

                        <?php

                        $image = get_field('middle_image_field_bottom_layer');

                        if (!empty($image)) : ?>

                            <img class="lazyload" data-src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                        <?php endif; ?>

                    </div>

                </div>

            </div>
        </div>

        <!-- Display on smaller than Desktop only -->

        <div class="row container-images show-me-on-mobile">
            <div class="col-xl-6">
                <!-- Add ACF middle_image_field_top_layer -->
                <div class="page-image-top-layer">

                    <?php

                    $image = get_field('middle_image_field_top_layer');

                    if (!empty($image)) : ?>

                        <img class="lazyload" data-src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                    <?php endif; ?>

                </div>
                <!-- Add ACF middle_image_field_bottom_layer -->
                <div class="page-image-bottom-layer">

                    <?php

                    $image = get_field('middle_image_field_bottom_layer');

                    if (!empty($image)) : ?>

                        <img class="lazyload" data-src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                    <?php endif; ?>

                </div>

            </div>

            <div class="col-xl-6">

                <div class="site-center">

                    <!-- CHANGE SITE ID TO CORRECT SITE; FALSE ID IS SELECTED ATM!!! -->
                    <h3 class="page-sub-heading-home" href="<?php echo get_page('about.php'); ?>"><?php _e('MACH DIR DIE HÄNDE SCHMUTZIG!', ''); ?></h3>
                    <!-- CHANGE SITE ID TO CORRECT SITE; FALSE ID IS SELECTED ATM!!! -->

                    <p>
                        <b>Mit einer Professionellen Ausstattung!</b>
                    </p>

                </div>

                <div class="page-text-field-layout-home">
                    <?php the_field('text_field_middle'); ?>
                </div>

            </div>

        </div>


        <!-- Third text / image part -->
        <!-- Add ACF bottom_image_field_top_layer -->


        <div class="site-center site-padding-top">
            <h1>GUT AUSGESTATTET</h1> <!-- Hardcoded sub-heading -->
            <hr class="site-main-underline"> <!-- Hardcoded underline -->
        </div>

        <div class="row container-images">
            <div class="col-xl-6">
                <div class="page-image-top-layer">

                    <?php

                    $image = get_field('bottom_image_field_top_layer');

                    if (!empty($image)) : ?>

                        <img class="lazyload" data-src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                    <?php endif; ?>

                </div>

                <!-- Add ACF bottom_image_field_bottom_layer -->
                <div class="page-image-bottom-layer">

                    <?php

                    $image = get_field('bottom_image_field_bottom_layer');

                    if (!empty($image)) : ?>

                        <img class="lazyload" data-src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                    <?php endif; ?>

                </div>
            </div>

            <!-- Add ACF text_field_bottom -->

            <div class="col-xl-6">
                <div class="site-center">

                    <!-- CHANGE SITE ID TO CORRECT SITE; FALSE ID IS SELECTED ATM!!! -->
                    <h3 class="page-sub-heading-home" href="<?php echo get_permalink(apply_filters('wpml_object_id', 4, 'post', true)); ?>"><?php _e('Technik und Bekleidung!', ''); ?></h3>
                    <!-- CHANGE SITE ID TO CORRECT SITE; FALSE ID IS SELECTED ATM!!! -->

                    <p>
                        <b>Von der Felge bis zum Helm ist alles machbar.</b>
                    </p>

                </div>

                <div class="page-text-field-layout-home">
                    <?php the_field('text_field_bottom'); ?>
                </div>
            </div>
        </div>

        <hr class="site-hr-footer">
    </div>
</main>
<?php get_footer(); ?>