<?php 

// Set google Fonts / Fonts (Monserrat in this case) tutorial at 'https://www.elmastudio.de/google-fonts-in-wordpress-themes/'



    function bellpali_fonts_url() {
        $fonts_url = '';
    
        /* Translators: If there are characters in your language that are not
        * supported by Monserrat, translate this to 'off'. Do not translate
        * into your own language.
        */
        $montserrat = _x( 'on', 'Montserrat font: on or off', 'bellpali' );
    
        if ( 'off' !== $montserrat ) {
            $font_families = array();
    
            if ( 'off' !== $montserrat ) {
                $font_families[] = 'Montserrat:400,700';
            }
    
            $query_args = array(
                'family' => urlencode( implode( '|', $font_families ) ),
                'subset' => urlencode( 'latin,latin-ext' ),
            );
    
            $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
        }
    
        return esc_url_raw( $fonts_url );
    }


    function bellpali_scripts_styles() {
        wp_enqueue_style( 'bellpali-fonts', bellpali_fonts_url(), array(), null );
    }
    add_action( 'wp_enqueue_scripts', 'bellpali_scripts_styles' );

//===========================================================================================================================================


// Nav menu

    add_action( 'after_setup_theme', 'bp_register_nav_menus');

    function bp_register_nav_menus() {
        register_nav_menu( 'nav_main', 'Navigation oben am Desktop' );
        register_nav_menu( 'nav_web_shop', 'Navigation oben am Desktop für Web Shop' );
        register_nav_menu( 'nav_categories', 'Navigation (Kategorien) neben Produkten' );
        register_nav_menu( 'sidebar_cart', 'Sidebar Warenkorb' );
        register_nav_menu( 'nav_shop_secondary', 'Shop Navigation Secondary' );
        // Navigation footer and Menu on mobile // positioned in the footer
        register_nav_menu( 'nav_footer', 'Navigation Footer' );
        register_nav_menu( 'nav_main__impressum-datenschutz', 'Impressum und Datenschutz Main Navigation mobile' );
    }



//===========================================================================================================================================


// Sidebar / Widgets

add_action( 'widgets_init', 'bp_register_sidebar' );

function bp_register_sidebar() {
    register_sidebar( array(
        'name'          => 'Sidebar Web Shop für Warenkorb',
        'id'            => 'sidebar_web_shop_cart', //initialize it where you want with '<?php dynamic_sidebar('sidebar_web_shop_cart'); ? >'
        'description'   => 'Sidebar auf der rechten Seite für den Webshop (Warenkorb Anzeige)',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widgettitle">',
        'after_title'   => '</h4>',
    ) );
}

  
//===========================================================================================================================================

// Custom Header

    function bellpali_custom_header_setup() {

        $args = array(
            'default-text-color' => '000',
            'width'              => 1800, // important or you can't cut the picture in the customizer 
            'height'             => 300,  // important or you can't cut the picture in the customizer 
            'flex-width'         => true,
            'flex-height'        => true,
            'uploads'            => true,
            'header-text'        => true,
        );

        add_theme_support( 'custom-header', $args );
    }
    add_action( 'after_setup_theme', 'bellpali_custom_header_setup' );


    $header_images = array(
        'bellpali' => array(
                'url'           => get_template_directory_uri() . '/images/banner_bellpali.jpg',
                'thumbnail_url' => get_template_directory_uri() . '/images/banner_bellpali_thumbnail.jpg',
                'description'   => 'Bellpali innen',
        ),
    );

    register_default_headers( $header_images );
    

//===========================================================================================================================================


// Custom Backgrounds // codex.wordpress.org/custom_backgrounds

    $defaults = array(
        'default-color' => '#000000'
    );

    add_theme_support('custom-background', $defaults); // let us use backgroundcolor and -image in the backend


//===========================================================================================================================================


// HTML5 

    $args = array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption'
    );
    
    add_theme_support('html5', $args);


//===========================================================================================================================================


// Register style sheet.

    function load_stylesheets() {

        wp_register_style( 'normalize', get_template_directory_uri() . '/css/normalize.css' );
        wp_enqueue_style( 'normalize' ); // import normalize.css

        wp_register_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css' );
        wp_enqueue_style( 'bootstrap' ); // import bootstrap.css

        wp_enqueue_style( 'font-awesome-free', '//use.fontawesome.com/releases/v5.6.1/css/all.css' ); // import font-awesome

        wp_register_style( 'flexslider', get_template_directory_uri() . '/css/flexslider.css' );
        wp_enqueue_style( 'flexslider' ); // import flexslider.css

        //wp_register_style( 'styles', get_template_directory_uri() . '/css/styles.php' );
		//wp_enqueue_style( 'styles' ); //import styles.php!
		//fixes by hsc solutions
		wp_enqueue_style('dynamic-css', admin_url('admin-ajax.php').'?action=dynamic_css');
        

        wp_register_style( 'app', get_template_directory_uri() . '/dist/css/app.css' );
        wp_enqueue_style( 'app' ); // import app.css (main .css file in this case)
    }

        add_action( 'wp_enqueue_scripts', 'load_stylesheets' );


//===========================================================================================================================================

// fixes dynamic styles
function dynaminc_css() {
       require(get_template_directory().'/css/styles.php');
       exit;
}
add_action('wp_ajax_dynamic_css', 'dynaminc_css');
add_action('wp_ajax_nopriv_dynamic_css', 'dynaminc_css');

// Register scripts.
    
    function load_scripts_js() {

        wp_register_script('custom', get_template_directory_uri() . '/js/index.js', array ( 'jquery' ), 1, true);
        wp_enqueue_script( 'custom');
        
        wp_register_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.js', array(), '', true );
        wp_enqueue_script( 'bootstrap');

        wp_register_script( 'jquery_easing', get_template_directory_uri() . '/js/jquery.easing.js', array(), '', true );
        wp_enqueue_script( 'jquery_easing');

        wp_register_script( 'flexslider', get_template_directory_uri() . '/js/jquery.flexslider.js', array(), '2.7.2', true );
        wp_enqueue_script( 'flexslider');

        wp_register_script( 'lazysizes', get_template_directory_uri() . '/js/lazysizes.min.js', array(), '', true );
        wp_enqueue_script( 'lazysizes');
            
    }
    
    
    add_action( 'wp_enqueue_scripts', 'load_scripts_js' );


//===========================================================================================================================================


// Image Thumbnails

    add_theme_support('post-thumbnails');
    add_image_size('slider', 1200, 840, true);
    add_image_size('slider_project', 1100, 740, true);
    add_image_size('slider_bikes', 1100, 740, true);



    add_action( 'woocommerce_before_checkout_form', 'bbloomer_cart_on_checkout_page_only', 5 );
 
    function bbloomer_cart_on_checkout_page_only() {
    
    if ( is_wc_endpoint_url( 'order-received' ) ) return;
    
    echo do_shortcode('[woocommerce_cart]');
    
    };


//===========================================================================================================================================


// add WooCommerce support -> tell woocommerce that your theme supports the plugin woocommerce.

    
    function woocommerce_support() {
        add_theme_support('woocommerce');
    }

    add_action('after_setup_theme', 'woocommerce_support');


//===========================================================================================================================================


// Add option in the backend ('WooCommerce' -> 'Einstellungen' -> [Reiter]'Produkte' -> [unter Reiter]'Produktanzahl pro Seite anpassen')

/**
 * Change number or products per row to 3
 */
    add_filter('loop_shop_columns', 'loop_columns', 999);
    if (!function_exists('loop_columns')) {
        function loop_columns() {
            $row = get_option('wc_num_of_products_per_row') ? get_option('wc_num_of_products_per_row') : 3; // if there is a number set, at wc_num_..., in the backend, use this number, if not, default (:) is 3.
            return $row; // n products per row
        }
    }

/**
 * Change number of products that are displayed per page (shop page)
 */
    add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

    function new_loop_shop_per_page( $cols ) {
    // $cols contains the current number of products per page based on the value stored on Options -> Reading
    // Return the number of products you wanna show per page.
    $cols = get_option('wc_num_of_products_per_page') ? get_option('wc_num_of_products_per_page') : 12; // if there is a number set, at wc_num_..., in the backend, use this number, if not, default (:) is 12.
    return $cols; // n products per page
    }


// Adding a section to a setting tab in WooCommerce backend (changing number of rows and shown products on page)

/**
 * Create the section beneath the products tab
 **/
    add_filter( 'woocommerce_get_sections_products', 'wc_page_configurator_add_section', 10, 1 ); // 'name of the filter', 'unique description for this filter (only one per project)'
    function wc_page_configurator_add_section( $sections ) {
        
        $sections['wc_page_configurator'] = __( 'Produktanzahl pro Seite anpassen', 'text-domain' );
        return $sections;   
    }

/**
 * Add settings to the specific section we created before
 */
    add_filter( 'woocommerce_get_settings_products', 'wc_page_configurator_all_settings', 10, 2 ); // 'name of the filter', 'unique description for this filter (only one per project)', priority (e.g. 1 highest, 2 second highest, and so on, most done with 10, 15, 20, and so on.)
    function wc_page_configurator_all_settings( $settings, $current_section ) {

/**
 * Check the current section is what we want
 **/

    if ( $current_section == 'wc_page_configurator' ) { // callback of unique id, must be the sam like beneath
        $settings_configurator = array();

        // Add Title to the Settings
        $settings_configurator[] = array( 
            'name'  => __( 'Produktanzahl pro Seite anpassen','text-domain' ), // Top heading
            'type'  => 'title',
            'desc'  => __( 'Die nachvolgenden Optionen dienen zur Anzeige der Menge an Produkte auf der Seite', 'text-domain' ), // Description for what does this option do
            'id'    => 'wc_page_configurator' ); // unique id
        
        // Add first checkbox option
        $settings_configurator[] = array(
            'name'     => __( 'Produktanzahl pro Reihe', 'text-domain' ), // Shown field description / name
            'desc_tip' => __( 'Ändert die Anzahl der gezeigten Produkte pro Reihe', 'text-domain' ), // Information section (little question mark in the circle)
            'id'       => 'wc_num_of_products_per_row', // unique id, which allows this option to get found and let it work in the backend
            'type'     => 'text', // what should be shown (in our case an textarea where you can put a number inside) 
            'css'      => 'min-width:300px;', // width of the shown textarea
            'desc'     => __( 'eine beliebige Zahl ( 3, 6, 9, 12, oder 4, 8, 12, 16 ist sinnvoll, aber kein muss.)', 'text-domain' ), // text behind the textarea (informations for what should be done in the textarea)
        );

        // Add second text field option
        $settings_configurator[] = array(
            'name'     => __( 'Produktanzahl pro Seite', 'text-domain' ), // Shown field description / name
            'desc_tip' => __( 'Ändert die Anzahl der gezeigten Produkte pro Seite', 'text-domain' ), // Information section (little question mark in the circle)
            'id'       => 'wc_num_of_products_per_page', // unique id, which allows this option to get found and let it work in the backend
            'type'     => 'text', // what should be shown (in our case an textarea where you can put a number inside)
            'desc'     => __( 'eine beliebige Zahl ', 'text-domain' ), // text behind the textarea (informations for what should be done in the textarea)
        );
        
        $settings_configurator[] = array(
            'type'  => 'sectionend',
            'id'    => 'wcslider' );
        return $settings_configurator;
    
/**
 * If not, return the standard settings
 **/
    } else {
        return $settings;
    }
}


//===========================================================================================================================================


// Woocommerce
// deactivate woocommerce wrapper
/*
    remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
    remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

    add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
    add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

    function my_theme_wrapper_start() {
        echo '<div class="page-web-shop-main-container">';
    }

    function my_theme_wrapper_end() {
        echo '</div>';
    }
*/

// generates function for if else statement -> usage for top navbar wether it is normal page or shop page
/*    function is_not_woocommerce() {
        if(!is_woocommerce() && !is_cart() && !is_checkout() && !is_account_page()) {
            return true;
        }

        return false;
    }
*/

    
// eliminates download tab in 'my Account'
    add_filter( 'woocommerce_account_menu_items', 'custom_remove_downloads_my_account', 999 );
 
    function custom_remove_downloads_my_account( $items ) {
    unset($items['downloads']);
    return $items;
    }
    
// changing height of textarea of WYSIWYG-Editor on front-page.php (Home in Backend) - layout purpose 
    add_action('acf/input/admin_head', 'my_acf_modify_wysiwyg_height');

    function my_acf_modify_wysiwyg_height() {
        
        ?>
        <style type="text/css">
/*
*   WYSIWYG-Editor Home (Startseite) Backend
*   Lower than 30px is not possible in the layout
*/
            .acf-field-5db972fce035a iframe,
            .acf-field-5db9ab579173e iframe,
            .acf-field-5db9732ee035b iframe,
            .acf-field-5db97357e035c iframe,
            .acf-field-5db9732ee035d iframe,
            .acf-field-5db857953b9b2 iframe,
            .acf-field-5db857a63b9b3 iframe {
                height: 30px !important;
            }

        </style>
        <?php    
        
    }
    
