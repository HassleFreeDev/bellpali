<?php
/**
 * default search form
 */
?>

<form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" id="search-form">
    <div class="search-wrap">
        <label class="screen-reader-text" for="s"></label>
        <input type="search" placeholder="<?php echo esc_attr( 'Produktsuche...', 'presentation' ); ?>" name="s" id="search-input" value="<?php echo esc_attr( get_search_query() ); ?>" />
        <input type="submit" id="search-submit" alt="Search" src="<?php bloginfo( 'template_url' ); ?>/images/search.png" />
    </div>
</form>