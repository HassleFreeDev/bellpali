<!DOCTYPE html>
<html <?php bloginfo('language') ?>>
    <head>
        <!-- class="site-..." counts for the whole layout -->
        <!-- class="page-..." counts for an specific page part -->
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php wp_title(''); ?> - <?php bloginfo('name'); ?></title>

        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php wp_enqueue_script('jquery'); ?>
        <?php wp_enqueue_script('comment-reply'); ?>

        <?php wp_head(); ?>
    </head>

    <body <?php body_class(/* it is possible to add custom classes right here... like 'test', and so on */); ?>>

        <div class="page-container-header">

            <div class="container-burger show-me-on-mobile"> 
                <div id="burger-btn" class="burger">
                    <div class="line"></div>
                    <div class="line"></div>
                    <div class="line"></div>
                </div>
            </div>

            <img class="site-header aligncenter" src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" /> <!-- inserts headimage (image header) -->
        </div>

    <?php get_template_part('navi'); ?> <!-- insert template_part 'navi' -->    
    
        <?php if(!is_front_page()) { ?> <!-- di display slogane, if is not frontpage -->

                <a class="site-heading" href="<?php echo home_url('/'); ?>"> <!-- link to home site --></a>

        <?php } ?>
    
        <header class="site-header">

<!-- Thumbnails Facebook / Instagram / Youtube Start -->
            <div class="container-instagram">
                <span class="fab fa-instagram icon-instagram"></span>   <!-- Icon -->
                <a href="https://www.instagram.com/bellpali_motorradmanufaktur/" class="thumbnail-instagram">Zu den Bildern!</a>
            </div>
            <div class="container-youtube">
                <span class="fab fa-youtube icon-youtube"></span>       <!-- Icon -->
                <a href="https://www.youtube.com/channel/UCOeKgxJAiVoepowWB08sxHQ/videos" class="thumbnail-youtube">Zu den bewegten Bildern!</a>
            </div>
            <div class="container-facebook">
                <span class="fab fa-facebook icon-facebook"></span>     <!-- Icon -->
                <a href="https://www.facebook.com/Bellpali/" class="thumbnail-facebook">Zu den Neuigkeiten!</a>
            </div>
<!-- Thumbnails Facebook / Instagram / Youtube End -->

        </header>
    
        
