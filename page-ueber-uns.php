<?php get_header(); ?>
<main>

    <div class="site-container">
        <div class="site-center site-padding-top">
            <h1>ÜBER UNS</h1> <!-- Hardcoded sub-heading -->
            <hr class="site-main-underline"> <!-- Hardcoded underline -->
        </div>


        <?php if (is_page('ueber-uns')) { /* insert team-employees.php if is page-ueber-uns.php*/
            get_template_part('template_parts/team', 'employees');
        };
        ?>


        <div class="site-center site-padding-top">
            <h1>UNTERNEHMEN</h1> <!-- Hardcoded sub-heading -->
            <hr class="site-main-underline"> <!-- Hardcoded underline -->
        </div>

        <div class="page-layout-company-description">
            <p><?php the_field('description_company'); ?></p>
        </div>

        <div class="show-me-on-desktop">
            <div class="erfahre-mehr-btn">
                <span class="give-me-more" id="slide-btn-about">Erfahre Mehr <div class="plus-icon"></div></span>
            </div>
        </div>

        <div class="show-me-on-mobile">
            <div class="erfahre-mehr-mobile">
                <span class="give-me-more-mobile" id="slide-btn-about-mobile">Erfahre Mehr</span>
                
            </div>
            
            <div class="page-layout-company-description-hidden">
                    <p><?php the_field('description_company_hidden'); ?></p>
                </div>
        </div>

        <div id="learn-more-box" class="learn-more-box">
            <hr class="site-main-underline site-margin-bot show-me-on-desktop"> <!-- Hardcoded underline -->

            <div class="page-layout-company-description">
                <p><?php the_field('description_company_hidden'); ?></p>
            </div>

            <hr class="site-main-underline site-margin-top"> <!-- Hardcoded underline -->
        </div>

        <div class="show-me-on-desktop">
            <div class="site-center site-padding-top">
                <h1>DER UMBAU</h1> <!-- Hardcoded sub-heading -->
                <hr class="site-main-underline"> <!-- Hardcoded underline -->
                <h5><b>Von einer alten Lagerhalle zu deiner Wohlfühloase.</b></h5>
            </div>

            <?php if (is_page('ueber-uns')) { /* insert slider-service.php if is page-22.php*/
                get_template_part('template_parts/slider', 'about');
            };
            ?>
        </div>
    </div>
</main>

<hr class="site-hr-footer">

<?php get_footer(); ?>