

    <div id="site-nav-main" class="show-me-on-desktop">
        <?php // generates menu where you can change the shown sites (in the nav element) in the backend

            $args = array(
                'theme_location'    => 'nav_main',
                'depth'             => 0, // how deep is an list shown like 'program -> new -> romantic' - it is only 'program -> new' shown
                'container'         => '',
                'menu_class'        => 'site-main-nav' //set an class you can work with css
            );


            wp_nav_menu($args); ?>
    </div>

    <div class="site-nav-layout-background-hidden">
        <div id="site-nav-main-mobile-hidden" class="show-me-on-mobile site-nav-main-mobile-hidden">

            <div id="close-btn" class="close-btn"></div>
            <?php // generates menu where you can change the shown sites (in the nav element) in the backend

                $args = array(
                    'theme_location'    => 'nav_main',
                    'depth'             => 0, // how deep is an list shown like 'program -> new -> romantic' - it is only 'program -> new' shown
                    'container'         => '',
                    'menu_class'        => 'site-main-nav-mobile'  //set an class you can work with css
                );


                wp_nav_menu($args); ?>

            <?php // generates menu where you can change the shown sites (in the nav element) in the backend

                $args = array(
                    'theme_location'    => 'nav_main__impressum-datenschutz',
                    'depth'             => 0, // how deep is an list shown like 'program -> new -> romantic' - it is only 'program -> new' shown
                    'container'         => '',
                    'menu_class'        => 'site-main-nav-mobile site-main-nav-mobile__impressum-datenschutz'  //set an class you can work with css
                );


                wp_nav_menu($args); ?>

        </div>


    </div>

    </nav>