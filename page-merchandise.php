<?php get_header(); ?>
<main>

<div class="site-container">
    <div class="site-center site-padding-top">
        <h1>Merchandise</h1> <!-- Hardcoded sub-heading -->
        <hr class="site-main-underline"> <!-- Hardcoded underline -->
    </div>


    <?php if (is_page('merchandise')) { /* insert team-employees.php if is page-ueber-uns.php*/
        get_template_part('template_parts/content', 'merchandise');
    };
    ?>

</main>

<hr class="site-hr-footer">

<?php get_footer(); ?>