<?php get_header(); ?>

<main>

    <div class="site-container page-height-cover">
        <div class="site-center site-padding-top">
            <h1>Datenschutzbelehrung</h1> <!-- Hardcoded sub-heading -->
            <hr class="site-main-underline"> <!-- Hardcoded underline -->
        </div>

        <?php the_field('datenschutz'); ?>

    </div>

</main>

<?php get_footer(); ?>